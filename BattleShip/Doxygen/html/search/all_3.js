var searchData=
[
  ['databaselogic',['DatabaseLogic',['../class_battle_ship_1_1_database_logic_1_1_database_logic.html',1,'BattleShip::DatabaseLogic']]],
  ['delete',['Delete',['../interface_i_repository.html#a38cd58aab386b1a5bce0dc7b18934d93',1,'IRepository.Delete()'],['../class_repository_1_1_repository.html#af23393227d3c1a479a5b2461c1c51827',1,'Repository.Repository.Delete()']]],
  ['destroyer',['Destroyer',['../namespace_battle_ship_1_1_models.html#a4096b0b0a9084bb8ef62f0a5d2f69c94af90034e6810bf57cdb3d99a298221fc9',1,'BattleShip::Models']]],
  ['direction',['Direction',['../interface_battle_ship_1_1_models_1_1_i_ship.html#a3707e80397236bdaff509919ed62a1d7',1,'BattleShip.Models.IShip.Direction()'],['../class_battle_ship_1_1_models_1_1_ship.html#af732de643e891fd602163d5d599ec60c',1,'BattleShip.Models.Ship.Direction()'],['../namespace_battle_ship_1_1_models.html#a47f95f67398781fceafa657ddf987df4',1,'BattleShip.Models.Direction()']]],
  ['down',['down',['../namespace_battle_ship_1_1_models.html#a47f95f67398781fceafa657ddf987df4a74e8333ad11685ff3bdae589c8f6e34d',1,'BattleShip::Models']]]
];
