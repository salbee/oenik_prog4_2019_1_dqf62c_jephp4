var searchData=
[
  ['score',['Score',['../class_battle_ship_1_1_logic_1_1_game_logic.html#ad52e7e41d3c375c44bbe72cd8d02b913',1,'BattleShip::Logic::GameLogic']]],
  ['setpropertyvalue',['SetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.SetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, object value, System.Globalization.CultureInfo culture)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.SetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, object value, System.Globalization.CultureInfo culture)']]],
  ['ship',['Ship',['../class_battle_ship_1_1_models_1_1_ship.html#a3cb5b266437bf34f5556451716107dd9',1,'BattleShip.Models.Ship.Ship(ShipClass shipClass)'],['../class_battle_ship_1_1_models_1_1_ship.html#a5d22438b4f632584ebf9cb65027b0435',1,'BattleShip.Models.Ship.Ship()']]],
  ['shipplaced',['ShipPlaced',['../class_battle_ship_1_1_logic_1_1_game_logic.html#af83dc7239a6604e1ddfbc38fc2d3bdd8',1,'BattleShip::Logic::GameLogic']]],
  ['shot',['Shot',['../class_battle_ship_1_1_logic_1_1_game_logic.html#ac976f1aafeb74f722bfbfa98d827963f',1,'BattleShip::Logic::GameLogic']]]
];
