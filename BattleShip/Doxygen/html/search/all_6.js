var searchData=
[
  ['gamelogic',['GameLogic',['../class_battle_ship_1_1_logic_1_1_game_logic.html',1,'BattleShip.Logic.GameLogic'],['../class_battle_ship_1_1_logic_1_1_game_logic.html#a55f40947ba8556f2308b12798b1afc80',1,'BattleShip.Logic.GameLogic.GameLogic()']]],
  ['gamelogictest',['GameLogicTest',['../class_test_1_1_game_logic_test.html',1,'Test']]],
  ['gamemodel',['GameModel',['../class_battle_ship_1_1_game_models_1_1_game_model.html',1,'BattleShip::GameModels']]],
  ['generatedinternaltypehelper',['GeneratedInternalTypeHelper',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html',1,'XamlGeneratedNamespace']]],
  ['genericrepository',['GenericRepository',['../class_repository_1_1_generic_repository.html',1,'Repository.GenericRepository'],['../class_repository_1_1_generic_repository.html#a4c32633b273a5ce317f8d678f82d2ed6',1,'Repository.GenericRepository.GenericRepository()']]],
  ['getall',['GetAll',['../interface_i_repository.html#a0e60e5f18f8ce9b964500bdec3edf9d9',1,'IRepository.GetAll()'],['../class_repository_1_1_repository.html#aa1c14d04818f5142c8f4cd8d104113b6',1,'Repository.Repository.GetAll()']]],
  ['getpropertyvalue',['GetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#afdc9fe15b56607d02082908d934480c6',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.GetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, System.Globalization.CultureInfo culture)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#afdc9fe15b56607d02082908d934480c6',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.GetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, System.Globalization.CultureInfo culture)']]]
];
