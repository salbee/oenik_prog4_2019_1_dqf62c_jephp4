var interface_battle_ship_1_1_models_1_1_i_field =
[
    [ "FieldPiece", "interface_battle_ship_1_1_models_1_1_i_field.html#a69783f3d88bf0694b12d86b8f35041fc", null ],
    [ "IsEnemy", "interface_battle_ship_1_1_models_1_1_i_field.html#aa702e2687485792ca1931e3e334475bf", null ],
    [ "IsShot", "interface_battle_ship_1_1_models_1_1_i_field.html#a17b07bd275198841f4ed6ac0c3270d87", null ],
    [ "ShipId", "interface_battle_ship_1_1_models_1_1_i_field.html#a73a808f4f8105cd7d30b2ef3df95aace", null ],
    [ "X", "interface_battle_ship_1_1_models_1_1_i_field.html#aa57bb0f43f9faf4509c6e5745d059824", null ],
    [ "Y", "interface_battle_ship_1_1_models_1_1_i_field.html#ae1a71bc459fa0d1693c4d415f28ba93d", null ]
];