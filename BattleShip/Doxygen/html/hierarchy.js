var hierarchy =
[
    [ "Application", null, [
      [ "BattleShip.App", "class_battle_ship_1_1_app.html", null ],
      [ "BattleShip.App", "class_battle_ship_1_1_app.html", null ],
      [ "BattleShip.App", "class_battle_ship_1_1_app.html", null ]
    ] ],
    [ "ApplicationSettingsBase", null, [
      [ "BattleShip.Properties.Settings", "class_battle_ship_1_1_properties_1_1_settings.html", null ]
    ] ],
    [ "BattleShip.DatabaseLogic.DatabaseLogic", "class_battle_ship_1_1_database_logic_1_1_database_logic.html", null ],
    [ "DbContext", null, [
      [ "BattleShip.Data.BattleShipDataBaseEntities", "class_battle_ship_1_1_data_1_1_battle_ship_data_base_entities.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "BattleShip.GameModels.GameModel", "class_battle_ship_1_1_game_models_1_1_game_model.html", null ]
    ] ],
    [ "Test.GameLogicTest", "class_test_1_1_game_logic_test.html", null ],
    [ "Repository.GenericRepository", "class_repository_1_1_generic_repository.html", null ],
    [ "BattleShip.DatabaseLogic.HighScoreByPlayer", "class_battle_ship_1_1_database_logic_1_1_high_score_by_player.html", null ],
    [ "BattleShip.Data.HighScores", "class_battle_ship_1_1_data_1_1_high_scores.html", null ],
    [ "BattleShip.ViewModels.HighScoreViewModel", "class_battle_ship_1_1_view_models_1_1_high_score_view_model.html", null ],
    [ "IComponentConnector", null, [
      [ "BattleShip.HelpWindow", "class_battle_ship_1_1_help_window.html", null ],
      [ "BattleShip.HelpWindow", "class_battle_ship_1_1_help_window.html", null ],
      [ "BattleShip.HighScoreWindow", "class_battle_ship_1_1_high_score_window.html", null ],
      [ "BattleShip.HighScoreWindow", "class_battle_ship_1_1_high_score_window.html", null ],
      [ "BattleShip.LoginWindow", "class_battle_ship_1_1_login_window.html", null ],
      [ "BattleShip.LoginWindow", "class_battle_ship_1_1_login_window.html", null ],
      [ "BattleShip.MainWindow", "class_battle_ship_1_1_main_window.html", null ],
      [ "BattleShip.MainWindow", "class_battle_ship_1_1_main_window.html", null ],
      [ "BattleShip.MenuWindow", "class_battle_ship_1_1_menu_window.html", null ],
      [ "BattleShip.MenuWindow", "class_battle_ship_1_1_menu_window.html", null ]
    ] ],
    [ "BattleShip.Models.IField", "interface_battle_ship_1_1_models_1_1_i_field.html", [
      [ "BattleShip.Models.Field", "class_battle_ship_1_1_models_1_1_field.html", null ]
    ] ],
    [ "BattleShip.IGameLogic", "interface_battle_ship_1_1_i_game_logic.html", [
      [ "BattleShip.Logic.GameLogic", "class_battle_ship_1_1_logic_1_1_game_logic.html", null ]
    ] ],
    [ "BattleShip.GameModels.IGameModel", "interface_battle_ship_1_1_game_models_1_1_i_game_model.html", [
      [ "BattleShip.GameModels.GameModel", "class_battle_ship_1_1_game_models_1_1_game_model.html", null ]
    ] ],
    [ "INotifyPropertyChanged", null, [
      [ "BattleShip.Logic.GameLogic", "class_battle_ship_1_1_logic_1_1_game_logic.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "IRepository< T >", "interface_i_repository.html", [
      [ "Repository.Repository< T >", "class_repository_1_1_repository.html", null ]
    ] ],
    [ "BattleShip.Models.IShip", "interface_battle_ship_1_1_models_1_1_i_ship.html", [
      [ "BattleShip.Models.Ship", "class_battle_ship_1_1_models_1_1_ship.html", null ]
    ] ],
    [ "BattleShip.ViewModels.LoginViewModel", "class_battle_ship_1_1_view_models_1_1_login_view_model.html", null ],
    [ "BattleShip.ViewModels.MainViewModel", "class_battle_ship_1_1_view_models_1_1_main_view_model.html", null ],
    [ "BattleShip.Data.Players", "class_battle_ship_1_1_data_1_1_players.html", null ],
    [ "BattleShip.Properties.Resources", "class_battle_ship_1_1_properties_1_1_resources.html", null ],
    [ "BattleShip.Data.SavedGames", "class_battle_ship_1_1_data_1_1_saved_games.html", null ],
    [ "Window", null, [
      [ "BattleShip.HelpWindow", "class_battle_ship_1_1_help_window.html", null ],
      [ "BattleShip.HelpWindow", "class_battle_ship_1_1_help_window.html", null ],
      [ "BattleShip.HelpWindow", "class_battle_ship_1_1_help_window.html", null ],
      [ "BattleShip.HighScoreWindow", "class_battle_ship_1_1_high_score_window.html", null ],
      [ "BattleShip.HighScoreWindow", "class_battle_ship_1_1_high_score_window.html", null ],
      [ "BattleShip.HighScoreWindow", "class_battle_ship_1_1_high_score_window.html", null ],
      [ "BattleShip.LoginWindow", "class_battle_ship_1_1_login_window.html", null ],
      [ "BattleShip.LoginWindow", "class_battle_ship_1_1_login_window.html", null ],
      [ "BattleShip.LoginWindow", "class_battle_ship_1_1_login_window.html", null ],
      [ "BattleShip.MainWindow", "class_battle_ship_1_1_main_window.html", null ],
      [ "BattleShip.MainWindow", "class_battle_ship_1_1_main_window.html", null ],
      [ "BattleShip.MainWindow", "class_battle_ship_1_1_main_window.html", null ],
      [ "BattleShip.MenuWindow", "class_battle_ship_1_1_menu_window.html", null ],
      [ "BattleShip.MenuWindow", "class_battle_ship_1_1_menu_window.html", null ],
      [ "BattleShip.MenuWindow", "class_battle_ship_1_1_menu_window.html", null ]
    ] ]
];