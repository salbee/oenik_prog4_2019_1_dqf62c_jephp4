var namespace_battle_ship_1_1_data =
[
    [ "BattleShipDataBaseEntities", "class_battle_ship_1_1_data_1_1_battle_ship_data_base_entities.html", "class_battle_ship_1_1_data_1_1_battle_ship_data_base_entities" ],
    [ "HighScores", "class_battle_ship_1_1_data_1_1_high_scores.html", "class_battle_ship_1_1_data_1_1_high_scores" ],
    [ "Players", "class_battle_ship_1_1_data_1_1_players.html", "class_battle_ship_1_1_data_1_1_players" ],
    [ "SavedGames", "class_battle_ship_1_1_data_1_1_saved_games.html", "class_battle_ship_1_1_data_1_1_saved_games" ]
];