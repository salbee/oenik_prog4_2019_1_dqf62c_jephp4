var class_test_1_1_game_logic_test =
[
    [ "Setup", "class_test_1_1_game_logic_test.html#a7c364ea98be43bcfa66086efc1c07475", null ],
    [ "Test_AI_SHOT", "class_test_1_1_game_logic_test.html#a5229bb05ff4af4a5c6314b85ac7f439b", null ],
    [ "Test_Rotate_Shuold_RotateTheShip_Clockwise", "class_test_1_1_game_logic_test.html#afbb18bc0d94adeeb6caa9bb43b831596", null ],
    [ "Test_Score", "class_test_1_1_game_logic_test.html#a9e45ba04f2c37aee102723ae07b48a78", null ],
    [ "Test_ShipPlaced_Down_Exception", "class_test_1_1_game_logic_test.html#a647c5b1c224e987ab2acc57eb6a40255", null ],
    [ "Test_ShipPlaced_Down_Succesfull", "class_test_1_1_game_logic_test.html#a22cb4ae0a8a7a62a596ba37446407623", null ],
    [ "Test_ShipPlaced_Left_Exception", "class_test_1_1_game_logic_test.html#a61ae1a1d55422e9d40013e990a3279c3", null ],
    [ "Test_ShipPlaced_Left_Succesfull", "class_test_1_1_game_logic_test.html#a2124211c329e56222bd651ca846a7f18", null ],
    [ "Test_ShipPlaced_Right_Exception", "class_test_1_1_game_logic_test.html#a8fd3574f10068e079a313505d093b768", null ],
    [ "Test_ShipPlaced_Right_Sucessfull", "class_test_1_1_game_logic_test.html#a9c76aaa138d96aef8631adddc81c60eb", null ],
    [ "Test_ShipPlaced_Ship_Collision", "class_test_1_1_game_logic_test.html#a2eca63ef21439169d530859172897eb0", null ],
    [ "Test_ShipPlaced_UP_Exception", "class_test_1_1_game_logic_test.html#adea908fee26a9a940f8d9d76ae9337c8", null ],
    [ "Test_ShipPlaced_UP_Sucessfull", "class_test_1_1_game_logic_test.html#a1aab892c18418edff0a46e5e36790004", null ],
    [ "Test_Shot", "class_test_1_1_game_logic_test.html#a9c1fa456806a34ddf491ae749e11ae73", null ]
];