var namespace_battle_ship =
[
    [ "Data", "namespace_battle_ship_1_1_data.html", "namespace_battle_ship_1_1_data" ],
    [ "DatabaseLogic", "namespace_battle_ship_1_1_database_logic.html", "namespace_battle_ship_1_1_database_logic" ],
    [ "GameModels", "namespace_battle_ship_1_1_game_models.html", "namespace_battle_ship_1_1_game_models" ],
    [ "Logic", "namespace_battle_ship_1_1_logic.html", "namespace_battle_ship_1_1_logic" ],
    [ "Models", "namespace_battle_ship_1_1_models.html", "namespace_battle_ship_1_1_models" ],
    [ "Properties", "namespace_battle_ship_1_1_properties.html", "namespace_battle_ship_1_1_properties" ],
    [ "ViewModels", "namespace_battle_ship_1_1_view_models.html", "namespace_battle_ship_1_1_view_models" ],
    [ "App", "class_battle_ship_1_1_app.html", "class_battle_ship_1_1_app" ],
    [ "HelpWindow", "class_battle_ship_1_1_help_window.html", "class_battle_ship_1_1_help_window" ],
    [ "HighScoreWindow", "class_battle_ship_1_1_high_score_window.html", "class_battle_ship_1_1_high_score_window" ],
    [ "IGameLogic", "interface_battle_ship_1_1_i_game_logic.html", "interface_battle_ship_1_1_i_game_logic" ],
    [ "LoginWindow", "class_battle_ship_1_1_login_window.html", "class_battle_ship_1_1_login_window" ],
    [ "MainWindow", "class_battle_ship_1_1_main_window.html", "class_battle_ship_1_1_main_window" ],
    [ "MenuWindow", "class_battle_ship_1_1_menu_window.html", "class_battle_ship_1_1_menu_window" ]
];