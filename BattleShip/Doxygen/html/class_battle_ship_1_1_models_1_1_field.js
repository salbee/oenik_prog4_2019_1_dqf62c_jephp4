var class_battle_ship_1_1_models_1_1_field =
[
    [ "Field", "class_battle_ship_1_1_models_1_1_field.html#a3f2ed5f960e0eadd5e8b8deeab3f4808", null ],
    [ "FieldPiece", "class_battle_ship_1_1_models_1_1_field.html#a9dcdcb60dfb8cd40f3100118f69c7f2a", null ],
    [ "IsEnemy", "class_battle_ship_1_1_models_1_1_field.html#a4a3aad9f8ce223cbf78a448834f215a8", null ],
    [ "IsShot", "class_battle_ship_1_1_models_1_1_field.html#ac0a70d5dd8a2dc2debe4079897b50e35", null ],
    [ "ShipId", "class_battle_ship_1_1_models_1_1_field.html#aba093baeaf002acece0910cf086f3bda", null ],
    [ "X", "class_battle_ship_1_1_models_1_1_field.html#abd0915591457e02a9a6a2fd667376dd8", null ],
    [ "Y", "class_battle_ship_1_1_models_1_1_field.html#a7d98b8cc6a625dba55ec1cf3806089a2", null ]
];