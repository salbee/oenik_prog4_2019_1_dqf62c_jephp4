var class_battle_ship_1_1_database_logic_1_1_database_logic =
[
    [ "DatabaseLogic", "class_battle_ship_1_1_database_logic_1_1_database_logic.html#a20e18df7422c71b4778335ccec0777d5", null ],
    [ "AddHighscore", "class_battle_ship_1_1_database_logic_1_1_database_logic.html#ab9efd669ddd5cadb58d1a18dd363a970", null ],
    [ "GetHighScores", "class_battle_ship_1_1_database_logic_1_1_database_logic.html#a889665a3541941cfd1a647e219793a48", null ],
    [ "GetHighScoresByPlayer", "class_battle_ship_1_1_database_logic_1_1_database_logic.html#af9f0a2c354bbfa569ce69fe314a5c7d7", null ],
    [ "GetPlayers", "class_battle_ship_1_1_database_logic_1_1_database_logic.html#a9ab0726bf396d1417021dd563306fa62", null ],
    [ "GetSavedgames", "class_battle_ship_1_1_database_logic_1_1_database_logic.html#a781ee9ccc4eda50cf03da1433c989ef3", null ],
    [ "GetUserByName", "class_battle_ship_1_1_database_logic_1_1_database_logic.html#ab9c2cd1590e6d43353cd2594ae259d0b", null ],
    [ "Insert", "class_battle_ship_1_1_database_logic_1_1_database_logic.html#a4b978d86ce394d1878750ae7bffcccc3", null ],
    [ "Update", "class_battle_ship_1_1_database_logic_1_1_database_logic.html#a096379fde60fe178ec88ce26fda8c6f3", null ]
];