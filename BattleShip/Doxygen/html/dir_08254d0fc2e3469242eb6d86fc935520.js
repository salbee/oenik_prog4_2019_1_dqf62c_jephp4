var dir_08254d0fc2e3469242eb6d86fc935520 =
[
    [ "GameModels", "dir_3101086bff14f007e3e2cdcbd924a6a5.html", "dir_3101086bff14f007e3e2cdcbd924a6a5" ],
    [ "Logic", "dir_d445d0e514934d777b52bd231ed14c47.html", "dir_d445d0e514934d777b52bd231ed14c47" ],
    [ "Models", "dir_3530fc15fc98a8770222d379405ba620.html", "dir_3530fc15fc98a8770222d379405ba620" ],
    [ "obj", "dir_7fe5a984c4b688a90ac25b25e054a268.html", "dir_7fe5a984c4b688a90ac25b25e054a268" ],
    [ "Properties", "dir_86fcbd1da0183a91a7d8d76e7fc2caa8.html", "dir_86fcbd1da0183a91a7d8d76e7fc2caa8" ],
    [ "ViewModels", "dir_96d0764e3d1e6743e68b8244be9426d4.html", "dir_96d0764e3d1e6743e68b8244be9426d4" ],
    [ "App.xaml.cs", "_app_8xaml_8cs_source.html", null ],
    [ "GlobalSuppressions.cs", "_battle_ship_2_global_suppressions_8cs_source.html", null ],
    [ "HelpWindow.xaml.cs", "_help_window_8xaml_8cs_source.html", null ],
    [ "HighScoreWindow.xaml.cs", "_high_score_window_8xaml_8cs_source.html", null ],
    [ "LoginWindow.xaml.cs", "_login_window_8xaml_8cs_source.html", null ],
    [ "MainWindow.xaml.cs", "_main_window_8xaml_8cs_source.html", null ],
    [ "MenuWindow.xaml.cs", "_menu_window_8xaml_8cs_source.html", null ]
];