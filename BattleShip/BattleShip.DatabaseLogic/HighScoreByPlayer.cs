﻿using BattleShip;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.DatabaseLogic
{
    /// <summary>
    /// Class for high score by player.
    /// </summary>
    public class HighScoreByPlayer
    {
        /// <summary>
        /// Gets or sets Place property.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public int Place { get; set; }

        /// <summary>
        /// Gets or sets Player's name property.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets Shots property.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public int Shots { get; set; }

        /// <summary>
        /// Gets or sets Elapsed seconds property.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public int ElapsedSeconds { get; set; }

        /// <summary>
        /// Gets score property.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public int Score { get => this.Shots * 10; }
    }
}
