﻿using BattleShip.Data;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.DatabaseLogic
{
    /// <summary>
    /// Class of database logic.
    /// </summary>
    public class DatabaseLogic
    {
        private static GenericRepository Repo { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseLogic"/> class.
        /// </summary>
        public DatabaseLogic()
        {
            Repo = new GenericRepository();
        }

        /// <summary>
        /// Gets all players.
        /// </summary>
        /// <returns>Players.</returns>
        public List<Players> GetPlayers()
        {
            return Repo.Players.GetAll().ToList();
        }

        /// <summary>
        /// Gets high scores.
        /// </summary>
        /// <returns>High scores.</returns>
        public List<HighScores> GetHighScores()
        {
            return Repo.HighScores.GetAll().ToList();
        }

        /// <summary>
        /// Inserts a new object.
        /// </summary>
        /// <param name="obj">Object.</param>
        public void Insert(object obj)
        {
            if (obj is Players)
            {
                Repo.Players.Insert(obj as Players);
            }
            else if (obj is HighScores)
            {
                Repo.HighScores.Insert(obj as HighScores);
            }
            else if (obj is SavedGames)
            {
                Repo.SavedGames.Insert(obj as SavedGames);
            }
        }

        /// <summary>
        /// Updates an object.
        /// </summary>
        /// <param name="obj">Object.</param>
        /// <param name="key">Key.</param>
        public void Update(object obj, object key)
        {
            if (obj is Players)
            {
                Repo.Players.Update(obj as Players, key);
            }
            else if (obj is HighScores)
            {
                Repo.HighScores.Update(obj as HighScores, key);
            }
            else if (obj is SavedGames)
            {
                Repo.SavedGames.Update(obj as SavedGames, key);
            }
        }

        /// <summary>
        /// Gets users by their names.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <returns>Users by their names.</returns>
        public Players GetUserByName(string name)
        {
            return Repo.Players.GetAll().FirstOrDefault(a => a.Name == name);
        }

        /// <summary>
        /// Adds high scores.
        /// </summary>
        /// <param name="userName">Name.</param>
        /// <param name="score">Score.</param>
        /// <param name="shotCnt">Shot's count.</param>
        public void AddHighscore(string userName, int score, int shotCnt)
        {
            int userId = GetUserByName(userName).ID;

            var highscore = new HighScores()
            {
                UserID = userId,
                ElapsedSeconds = score,
                Shots = shotCnt,
            };

            Repo.HighScores.Insert(highscore);
        }

        /// <summary>
        /// Gets high scores by players.
        /// </summary>
        /// <returns>High scores by players.</returns>
        public List<HighScoreByPlayer> GetHighScoresByPlayer()
        {
            return (from highScore in Repo.HighScores.GetAll()
                    join player in Repo.Players.GetAll() on highScore.UserID equals player.ID
                    select new
                    {
                        ElapsedSeconds = highScore.ElapsedSeconds,
                        Shots = highScore.Shots,
                        PlayerName = player.Name,
                    })
                    .OrderBy(x => x.Shots * x.ElapsedSeconds)
                    .ToList()
                    .Select((x, index) => new HighScoreByPlayer()
                    {
                        Place = index + 1,
                        ElapsedSeconds = x.ElapsedSeconds,
                        Shots = x.Shots,
                        PlayerName = x.PlayerName,
                    }).ToList();
        }
    }
}
