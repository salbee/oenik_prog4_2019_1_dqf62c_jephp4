﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Moq;
using BattleShip.Models;
using BattleShip.Logic;
using Repository;
using BattleShip.DatabaseLogic;
using BattleShip.Data;

namespace Test
{
    [TestFixture]
    public class GameLogicTest
    {
        
        GameLogic gameLogic;
        Ship ship;
        Mock<IField> mockField;
        Mock<IShip> mockShip;
        Mock<IRepository<object>> mockRepo;
        Mock<DatabaseLogic> mockData;

        [SetUp]
        public void Setup()
        {
            mockField = new Mock<IField>();
            mockShip = new Mock<IShip>();
            gameLogic = new GameLogic();
            mockRepo = new Mock<IRepository<object>>();
            mockData = new Mock<DatabaseLogic>();

        }
        [Test]
        public void Test_Rotate_Shuold_RotateTheShip_Clockwise()
        {
            ship = new Ship(ShipClass.Boat);
            gameLogic.Rotate(ship);
            Assert.That(ship.Direction.Equals(Direction.right));
            gameLogic.Rotate(ship);
            Assert.That(ship.Direction.Equals(Direction.down));
            gameLogic.Rotate(ship);
            Assert.That(ship.Direction.Equals(Direction.left));
            gameLogic.Rotate(ship);
            Assert.That(ship.Direction.Equals(Direction.up));
        }
        [Test]
        public void Test_ShipPlaced_Left_Exception()
        {
            string actual = null;
            gameLogic.ShipPlacedEvent += delegate (object sender, EventArgs e)
            {
                actual = sender.ToString();
            };

            mockField.Setup(t => t.X).Returns(0);
            mockField.Setup(t => t.Y).Returns(0);
            mockShip.Setup(t => t.Direction).Returns(Direction.left);           
            mockShip.Setup(t => t.Lenght).Returns(4);
            mockShip.Setup(t => t.Fields).Returns(new List<IField>());
            ship = new Ship(ShipClass.Boat);            
            gameLogic.ShipPlaced(gameLogic.gameBoard[0,0], ship, gameLogic.gameBoard);

            Assert.Null(actual);
           
        }
        [Test]
        public void Test_ShipPlaced_Left_Succesfull()
        {
            string actual = null;
            gameLogic.ShipPlacedEvent += delegate (object sender, EventArgs e)
            {
                actual = sender.ToString();
            };

            mockField.Setup(t => t.X).Returns(0);
            mockField.Setup(t => t.Y).Returns(0);
            mockShip.Setup(t => t.Direction).Returns(Direction.left);
            mockShip.Setup(t => t.Lenght).Returns(4);
            mockShip.Setup(t => t.Fields).Returns(new List<IField>());
            mockField.Setup(t => t.X).Returns(9);
            mockField.Setup(t => t.Y).Returns(9);
            gameLogic.ShipPlaced(mockField.Object, mockShip.Object, gameLogic.gameBoard);
            Assert.AreEqual("BattleShip.Logic.GameLogic", actual);
        }

        [Test]
        public void Test_ShipPlaced_UP_Exception()
        {
            string actual = null;
            gameLogic.ShipPlacedEvent += delegate (object sender, EventArgs e)
            {
                actual = sender.ToString();
            };            
            mockField.Setup(t => t.X).Returns(0);
            mockField.Setup(t => t.Y).Returns(0);
            mockShip.Setup(t => t.Direction).Returns(Direction.up);
            mockShip.Setup(t => t.Lenght).Returns(4);
            mockShip.Setup(t => t.Fields).Returns(new List<IField>());
            gameLogic.ShipPlaced(mockField.Object, mockShip.Object, gameLogic.gameBoard);
            Assert.Null(actual);
           
        }
        [Test]
        public void Test_ShipPlaced_UP_Sucessfull()
        {
            string actual = null;
            gameLogic.ShipPlacedEvent += delegate (object sender, EventArgs e)
            {
                actual = sender.ToString();
            };
            mockField.Setup(t => t.X).Returns(0);
            mockField.Setup(t => t.Y).Returns(0);
            mockShip.Setup(t => t.Direction).Returns(Direction.up);
            mockShip.Setup(t => t.Lenght).Returns(4);
            mockShip.Setup(t => t.Fields).Returns(new List<IField>());
            mockField.Setup(t => t.X).Returns(6);
            mockField.Setup(t => t.Y).Returns(5);
            gameLogic.ShipPlaced(mockField.Object, mockShip.Object, gameLogic.gameBoard);
            Assert.AreEqual("BattleShip.Logic.GameLogic", actual);
        }

        [Test]
        public void Test_ShipPlaced_Right_Exception()
        {
            string actual = null;
            gameLogic.ShipPlacedEvent += delegate (object sender, EventArgs e)
            {
                actual = sender.ToString();
            };
            mockField.Setup(t => t.X).Returns(9);
            mockField.Setup(t => t.Y).Returns(9);
            mockShip.Setup(t => t.Direction).Returns(Direction.right);
            mockShip.Setup(t => t.Lenght).Returns(4);
            mockShip.Setup(t => t.Fields).Returns(new List<IField>());
            gameLogic.ShipPlaced(mockField.Object, mockShip.Object, gameLogic.gameBoard);
            Assert.Null(actual);
          
        }
        [Test]
        public void Test_ShipPlaced_Right_Sucessfull()
        {
            string actual = null;
            gameLogic.ShipPlacedEvent += delegate (object sender, EventArgs e)
            {
                actual = sender.ToString();
            };
            mockField.Setup(t => t.X).Returns(9);
            mockField.Setup(t => t.Y).Returns(9);
            mockShip.Setup(t => t.Direction).Returns(Direction.right);
            mockShip.Setup(t => t.Lenght).Returns(4);
            mockShip.Setup(t => t.Fields).Returns(new List<IField>());
            mockField.Setup(t => t.X).Returns(0);
            mockField.Setup(t => t.Y).Returns(0);
            gameLogic.ShipPlaced(mockField.Object, mockShip.Object, gameLogic.gameBoard);
            Assert.AreEqual("BattleShip.Logic.GameLogic", actual);
        }
        [Test]
        public void Test_ShipPlaced_Down_Exception()
        {
            string actual = null;
            gameLogic.ShipPlacedEvent += delegate (object sender, EventArgs e)
            {
                actual = sender.ToString();
            };
            mockField.Setup(t => t.X).Returns(9);
            mockField.Setup(t => t.Y).Returns(9);
            mockShip.Setup(t => t.Direction).Returns(Direction.down);
            mockShip.Setup(t => t.Lenght).Returns(4);
            mockShip.Setup(t => t.Fields).Returns(new List<IField>());
            gameLogic.ShipPlaced(mockField.Object, mockShip.Object, gameLogic.gameBoard);
            Assert.Null(actual);           
            
            

        }
        [Test]
        public void Test_ShipPlaced_Down_Succesfull()
        {
            string actual = null;
            gameLogic.ShipPlacedEvent += delegate (object sender, EventArgs e)
            {
                actual = sender.ToString();
            };
            mockField.Setup(t => t.X).Returns(9);
            mockField.Setup(t => t.Y).Returns(9);
            mockShip.Setup(t => t.Direction).Returns(Direction.down);
            mockShip.Setup(t => t.Lenght).Returns(4);
            mockShip.Setup(t => t.Fields).Returns(new List<IField>());
            mockField.Setup(t => t.X).Returns(0);
            mockField.Setup(t => t.Y).Returns(0);
            gameLogic.ShipPlaced(mockField.Object, mockShip.Object, gameLogic.gameBoard);
            Assert.AreEqual("BattleShip.Logic.GameLogic", actual);
        }

        [Test]
        public void Test_ShipPlaced_Ship_Collision()
        {
            string actual = null;
            gameLogic.ShipPlacedEvent += delegate (object sender, EventArgs e)
            {
                actual = sender.ToString();
            };
            mockField.Setup(t => t.X).Returns(5);
            mockField.Setup(t => t.Y).Returns(5);
            mockShip.Setup(t => t.Direction).Returns(Direction.down);
            mockShip.Setup(t => t.Lenght).Returns(4);
            mockShip.Setup(t => t.Fields).Returns(new List<IField>());
            gameLogic.ShipPlaced(mockField.Object, mockShip.Object, gameLogic.gameBoard);
            Assert.AreEqual("BattleShip.Logic.GameLogic", actual);
            actual = null;
            mockField.Setup(t => t.X).Returns(5);
            mockField.Setup(t => t.Y).Returns(4);
            gameLogic.ShipPlaced(mockField.Object, mockShip.Object, gameLogic.gameBoard);
            Assert.Null(actual);

        }
        [Test]
        public void Test_Shot()
        {
            string actual = null;
            gameLogic.ShotEvent += delegate (object sender, EventArgs e)
            {
                actual = sender.ToString();
            };
            gameLogic.Shot(5, 5);
            Assert.True(gameLogic.AIBoard[5, 5].IsShot);
            actual = null;
            Assert.False(gameLogic.Shot(5, 5));            
            Assert.True(gameLogic.Shot(6, 6));
            
        }

        [Test]
        public void Test_AI_SHOT()
        {
            string actual = null;

            gameLogic.ShotEvent += delegate (object sender, EventArgs e)
            {
                actual = sender.ToString();
            };
            gameLogic = new GameLogic();
            gameLogic.ships[1].Direction = Direction.down;
            gameLogic.ShipPlaced(gameLogic.gameBoard[1, 2], gameLogic.ships[1], gameLogic.gameBoard);
            for (int i = 0; i < 20; i++)
            {
                gameLogic.AIShot();
            }
            Assert.AreEqual(gameLogic.ships[1].Life, 0);
            
            
        }        
        [Test]
        public void SaveRepo()
        {
            List<HighScores> hsc=new List<HighScores>();
            hsc.Add(new HighScores());
            hsc[0].Shots = 200;            
            //mockData.Setup(t => t.GetHighScores()).Returns(hsc);
            //Assert.AreEqual(hsc[0].Shots, gameLogic.Score());
           
        }
      
    }
}
