﻿// Enable XML documentation output
// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Repository
{
    using System;
    using System.Linq;
    using BattleShip.Data;

    /// <summary>
    /// Contains the database CRUD methods
    /// </summary>
    /// <typeparam name="T">Type of database entity class</typeparam>
    public class Repository<T> : IRepository<T>
        where T : class
    {
        private static BattleShipDataBaseEntities ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        public Repository()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="context"><see cref="SokobanDatabaseEntities"/> class, what every other repository shares.</param>
        public Repository(BattleShipDataBaseEntities context)
        {
            ctx = context;
        }

        /// <summary>
        /// Insert a new object to the table.
        /// The table is determined by the object's type.
        /// </summary>
        /// <param name="obj">The insertable object.</param>
        public void Insert(T obj)
        {
            ctx.Set<T>().Add(obj);
            ctx.SaveChanges();
        }

        /// <summary>
        /// Update a specified table object.
        /// The table is determined by the object's type.
        /// </summary>
        /// <param name="obj">The updated object.</param>
        /// <param name="key">The key of the updateable object.</param>
        public void Update(T obj, object key)
        {
            var old = ctx.Set<T>().Find(key);

            if (old != null)
            {
                ctx.Entry(old).CurrentValues.SetValues(obj);
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Delete an object from the table.
        /// The table is determined by the object's type.
        /// </summary>
        /// <param name="obj">The deletable object.</param>
        public void Delete(T obj)
        {
            ctx.Set<T>().Remove(obj);
            ctx.SaveChanges();
        }

        /// <summary>
        /// Get every row from a table
        /// </summary>
        /// <returns><see cref="IQueryable{T}"/></returns>
        public virtual IQueryable<T> GetAll() => ctx.Set<T>().AsQueryable();

        
    }
}
