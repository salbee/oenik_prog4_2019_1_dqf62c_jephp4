﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1633:The file header is missing or not located at the top of the file.", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1200:Using directive should appear within a namespace declaration", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1505:An opening brace should not be followed by a blank line.", Justification = "<Pending>", Scope = "namespace", Target = "~N:Repository")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1028:Code should not contain trailing whitespace", Justification = "<Pending>", Scope = "member", Target = "~P:Repository.GenericRepository.HighScores")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1507:Code should not contain multiple blank lines in a row", Justification = "<Pending>", Scope = "member", Target = "~P:Repository.GenericRepository.SavedGames")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1028:Code should not contain trailing whitespace", Justification = "<Pending>", Scope = "type", Target = "~T:Repository.GenericRepository")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1507:Code should not contain multiple blank lines in a row", Justification = "<Pending>", Scope = "type", Target = "~T:Repository.GenericRepository")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1507:Code should not contain multiple blank lines in a row", Justification = "<Pending>", Scope = "type", Target = "~T:Repository.Repository`1")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1508:A closing brace should not be preceded by a blank line.", Justification = "<Pending>", Scope = "type", Target = "~T:Repository.Repository`1")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1028:Code should not contain trailing whitespace", Justification = "<Pending>", Scope = "type", Target = "~T:Repository.Repository`1")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", Scope = "type", Target = "Repository.GenericRepository")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1518:Code should not contain blank lines at the end of the file", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1512:Single-line comments should not be followed by blank line", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1028:Code should not contain trailing whitespace", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1515:Single-line comment should be preceded by blank line", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1517:Code should not contain blank lines at start of file", Justification = "<Pending>")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1518:Code should not contain blank lines at the end of the file", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1512:Single-line comments should not be followed by blank line", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1028:Code should not contain trailing whitespace", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1515:Single-line comment should be preceded by blank line", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1517:Code should not contain blank lines at start of file", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1639:File header should have summary", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SX1309:Field 'ctx' should begin with an underscore", Justification = "<Pending>", Scope = "member", Target = "~F:Repository.GenericRepository.ctx")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SX1309S:Static field 'ctx' should begin with an underscore", Justification = "<Pending>", Scope = "member", Target = "~F:Repository.Repository`1.ctx")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SX1101:Do not prefix local calls with 'this.'", Justification = "<Pending>", Scope = "member", Target = "~M:Repository.GenericRepository.#ctor")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1639:File header should have summary", Justification = "<Pending>", Scope = "namespace", Target = "~N:Repository")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA2210:AssembliesShouldHaveValidStrongNames")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "BattleShip")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1014:MarkAssembliesWithClsCompliant")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1050:DeclareTypesInNamespaces", Scope = "type", Target = "IRepository`1")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "IRepository`1.#GetAll()")]

