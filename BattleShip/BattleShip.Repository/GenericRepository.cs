﻿// Enable XML documentation output
// <copyright file="GenericRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using BattleShip.Data;

namespace Repository
{
  

    /// <summary>
    /// Contains the database entity repositories
    /// </summary>
    public class GenericRepository
    {
        private readonly BattleShipDataBaseEntities ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository"/> class.
        /// Constructor, what creates the entity repositories with the same context
        /// </summary>
        public GenericRepository()
        {
            this.ctx = new BattleShipDataBaseEntities();
            this.HighScores = new Repository<HighScores>(this.ctx);
            this.SavedGames = new Repository<SavedGames>(this.ctx);
            this.Players = new Repository<Players>(this.ctx);
        }
        
        /// <summary>
        /// Gets or sets the getter and setter for highscores
        /// </summary>
        public IRepository<HighScores> HighScores { get; set; }


        /// <summary>
        /// Gets or sets the getter and setter for saved games
        /// </summary>
        public IRepository<SavedGames> SavedGames { get; set; }

        /// <summary>
        /// Gets or sets the getter and setter for users
        /// </summary>
        public IRepository<Players> Players { get; set; }
    }
}
