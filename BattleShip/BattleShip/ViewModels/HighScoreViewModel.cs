﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.Data;
using BattleShip.DatabaseLogic;

namespace BattleShip.ViewModels
{
    /// <summary>
    /// View model class for high scores.
    /// </summary>
    class HighScoreViewModel
    {
        /// <summary>
        /// Gets or sets high scores.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public List<HighScoreByPlayer> HighScores { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreViewModel"/> class.
        /// </summary>
        public HighScoreViewModel()
        {
            HighScores = App.Logic.GetHighScoreByPlayer();
        }
    }
}
