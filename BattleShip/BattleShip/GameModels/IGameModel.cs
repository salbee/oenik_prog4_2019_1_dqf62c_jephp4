﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace BattleShip.GameModels
{
    interface IGameModel
    {
        void DrawMap(DrawingContext DC);
    }
}
