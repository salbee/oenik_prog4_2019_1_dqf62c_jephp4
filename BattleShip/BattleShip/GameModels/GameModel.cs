﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using BattleShip.Logic;
using BattleShip.Models;

namespace BattleShip.GameModels
{
    /// <summary>
    /// Game model class.
    /// </summary>
    class GameModel : FrameworkElement, IGameModel
    {
        /// <summary>
        /// Gets or sets game logic.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public GameLogic GL
        {
            get
            {
                return (GameLogic)GetValue(GLProperty);
            }
            set
            {
                SetValue(GLProperty, value);
                GL.PropertyChanged += GL_GameStateChanged;
                GL.GameEndDefeat += GL_GameOverDefeat;
                GL.GameEndWin += GL_GameOverVictory;
            }
        }

        /// <summary>
        /// Dependency property.
        /// </summary>
        public static readonly DependencyProperty GLProperty = DependencyProperty.Register("MyProp", typeof(GameLogic), typeof(GameModel), new PropertyMetadata(null));
        
        /// <summary>
        /// Gets or sets selected ship.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public Ship selectedShip { get; set; }

        /// <summary>
        /// Gets or sets selected water field.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        private ImageBrush WaterField = new ImageBrush(new BitmapImage(new Uri("Images\\water.png", UriKind.RelativeOrAbsolute)));

        /// <summary>
        /// Gets or sets selected aimed water field.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        private ImageBrush AimedWaterField = new ImageBrush(new BitmapImage(new Uri("Images\\aimed_water.png", UriKind.RelativeOrAbsolute)));

        /// <summary>
        /// Gets or sets selected ship field.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        private ImageBrush ShipField = new ImageBrush(new BitmapImage(new Uri("Images\\ship.png", UriKind.RelativeOrAbsolute)));

        /// <summary>
        /// Gets or sets selected hit ship field.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        private ImageBrush HitShipField = new ImageBrush(new BitmapImage(new Uri("Images\\hit_ship.png", UriKind.RelativeOrAbsolute)));

        /// <summary>
        /// Gets or sets selected sinked ship field.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        private ImageBrush SinkedShipField = new ImageBrush(new BitmapImage(new Uri("Images\\sinked_ship.png", UriKind.RelativeOrAbsolute)));

        /// <summary>
        /// Gets or sets selected wall.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        private ImageBrush Wall = new ImageBrush(new BitmapImage(new Uri("Images\\wall.png", UriKind.RelativeOrAbsolute)));

        /// <summary>
        /// Gets or sets selected ship image.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        private ImageBrush ShipImg = new ImageBrush(new BitmapImage(new Uri("Images\\shipimg.png", UriKind.RelativeOrAbsolute)));

        /// <summary>
        /// Gets or sets selected ship.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        private ImageBrush ShipImgL = new ImageBrush(new BitmapImage(new Uri("Images\\shipimgl.png", UriKind.RelativeOrAbsolute)));

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        public GameModel()
        {
            selectedShip = new Ship();
            selectedShip.Lenght = 2;
            selectedShip.Direction = Direction.up;
            selectedShip.ShipClass = ShipClass.Boat;
        }

        /// <summary>
        /// Draws map.
        /// </summary>
        /// <param name="DC">Drawing context.</param>
        public void DrawMap(DrawingContext DC)
        {
            for (int i = 0; i < 10; i++)
            {
                DC.DrawRectangle(Wall, null, new Rect(400, i * 40, 40, 40 ));
            } 

            for (var i = 1; i <= GL.gameBoard.GetLength(0); i++)
            {
                for (var j = 1; j <= GL.gameBoard.GetLength(1); j++)
                {
                    if (GL.gameBoard[i - 1, j - 1].ShipId == 0 && GL.gameBoard[i - 1, j - 1].IsShot == true)
                    {
                        DC.DrawRectangle(AimedWaterField, null, GL.gameBoard[i - 1, j - 1].FieldPiece);
                    }
                    else if (GL.gameBoard[i - 1, j - 1].ShipId == 1 && GL.gameBoard[i - 1, j - 1].IsShot == true)
                    {
                        DC.DrawRectangle(HitShipField, null, GL.gameBoard[i - 1, j - 1].FieldPiece);
                    }
                    else if (GL.gameBoard[i - 1, j - 1].ShipId == 0 && GL.gameBoard[i - 1, j - 1].IsShot == false)
                    {                        
                        DC.DrawRectangle(WaterField, null, GL.gameBoard[i - 1, j - 1].FieldPiece);
                    }
                    else if (GL.gameBoard[i - 1, j - 1].ShipId == 1 && GL.gameBoard[i - 1, j - 1].IsShot == false)
                    {
                        DC.DrawRectangle(ShipField, null, GL.gameBoard[i - 1, j - 1].FieldPiece);
                    }
                    else
                    {
                        DC.DrawRectangle(SinkedShipField, null, GL.gameBoard[i - 1, j - 1].FieldPiece);
                    }
                }
            }

            for (var i = 1; i <= GL.AIBoard.GetLength(0); i++)
            {
                for (var j = 1; j <= GL.AIBoard.GetLength(1); j++)
                {
                    if (GL.AIBoard[i - 1, j - 1].IsShot == true && GL.AIBoard[i - 1, j - 1].ShipId == 0)
                    {
                        DC.DrawRectangle(AimedWaterField, null, GL.AIBoard[i - 1, j - 1].FieldPiece);
                    }
                    else if (GL.AIBoard[i - 1, j - 1].ShipId == 1 && GL.AIBoard[i - 1, j - 1].IsShot == true)
                    {
                        DC.DrawRectangle(HitShipField, null, GL.AIBoard[i - 1, j - 1].FieldPiece);
                    }
                    else if (GL.AIBoard[i - 1, j - 1].IsShot == false)
                    {
                        DC.DrawRectangle(WaterField, null, GL.AIBoard[i - 1, j - 1].FieldPiece);
                    }
                    else
                    {
                        DC.DrawRectangle(SinkedShipField, null, GL.AIBoard[i - 1, j - 1].FieldPiece);
                    }
                }
            }
        }

        
        /// <summary>
        /// draws ship.
        /// </summary>
        /// <param name="DC">drawing context.</param>
        public void DrawShip(DrawingContext DC)
        {           
            Rect ship = new Rect(GL.MouseX - 20, GL.MouseY - (60 + ((selectedShip.Lenght - 2) * 40)), 40, 80 + ((selectedShip.Lenght - 2) * 40));
            ;

            switch (selectedShip.Direction)
            {
                case Direction.up:
                    ship = new Rect(GL.MouseX - 20, GL.MouseY - (60 + ((selectedShip.Lenght - 2) * 40)), 40, 80 + ((selectedShip.Lenght - 2) * 40));
                    DC.DrawRectangle(ShipImg, null, ship);
                    
                    break;
                case Direction.right:
                    ship = new Rect(GL.MouseX - 20, GL.MouseY - 20, 80 + ((selectedShip.Lenght - 2) * 40), 40);
                    DC.DrawRectangle(ShipImgL, null, ship);
                    
                    break;
                case Direction.down:
                    ship = new Rect(GL.MouseX - 20, GL.MouseY - 20, 40, 80 + ((selectedShip.Lenght - 2) * 40));
                    DC.DrawRectangle(ShipImg, null, ship);
                   
                    break;
                case Direction.left:
                    ship = new Rect(GL.MouseX - (60 + ((selectedShip.Lenght - 2) * 40)), GL.MouseY - 20, 80 + ((selectedShip.Lenght - 2) * 40), 40);
                    DC.DrawRectangle(ShipImgL, null, ship);
                  
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Onrender method.
        /// </summary>
        /// <param name="drawingContext">Drawing context.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            DrawMap(drawingContext);

            if (selectedShip != null)
            {
                DrawShip(drawingContext);               
            }
        }

        /// <summary>
        /// Game is over by victory.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void GL_GameOverVictory(object sender, EventArgs e)
        {
            MessageBox.Show("Győztél \n\n Pontszám: " + GL.Score());
            /*var menu = new MenuWindow();
            menu.Show();*/
        }

        /// <summary>
        /// Game is over by defeat.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void GL_GameOverDefeat(object sender, EventArgs e)
        {
            MessageBox.Show("Sajnos vesztettél \n\n Pontszám: " + GL.Score());
            /*var menu = new MenuWindow();
            menu.Show();*/
        }

        /// <summary>
        /// Game's state changed.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void GL_GameStateChanged(object sender, EventArgs e)
        {
            this.InvalidateVisual();
        }
    }
}
