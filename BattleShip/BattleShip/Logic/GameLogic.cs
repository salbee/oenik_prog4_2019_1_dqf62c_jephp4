﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using BattleShip.DatabaseLogic;
using BattleShip.Models;

namespace BattleShip.Logic
{/// <summary>
 /// Game Logic class.
 /// </summary>
    public class GameLogic : IGameLogic , INotifyPropertyChanged
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// GameLogic class constructor.
        /// </summary>
        public GameLogic()
        {
            logic = new DatabaseLogic.DatabaseLogic();
            AIBoard = new Field[10, 10];
            gameBoard = new Field[10, 10];
            rightDirection = Direction.up;
            lastHitKordinate = new int[2];
            lastKordinate = new int[2];
            lastHitKordinate[0] = 0;
            lastHitKordinate[1] = 0;
            lastKordinate[0] = 0;
            lastKordinate[1] = 0;
            AIS = false;
            AISank = true;
            ships = new List<Ship>();
            AIShips = new List<Ship>();
            AiAliveShips = 5;
            aliveShips = 5;
            ships.Add(new Ship((ShipClass)Enum.Parse(typeof(ShipClass), "Boat")));
            ships.Add(new Ship((ShipClass)Enum.Parse(typeof(ShipClass), "Submarine")));
            ships.Add(new Ship((ShipClass)Enum.Parse(typeof(ShipClass), "Destroyer")));
            ships.Add(new Ship((ShipClass)Enum.Parse(typeof(ShipClass), "BattleShip")));
            ships.Add(new Ship((ShipClass)Enum.Parse(typeof(ShipClass), "AircraftCarrier")));

            AIShips.Add(new Ship((ShipClass)Enum.Parse(typeof(ShipClass), "Boat")));
            AIShips.Add(new Ship((ShipClass)Enum.Parse(typeof(ShipClass), "Submarine")));
            AIShips.Add(new Ship((ShipClass)Enum.Parse(typeof(ShipClass), "Destroyer")));
            AIShips.Add(new Ship((ShipClass)Enum.Parse(typeof(ShipClass), "BattleShip")));
            AIShips.Add(new Ship((ShipClass)Enum.Parse(typeof(ShipClass), "AircraftCarrier")));
            //játék tér először minden víz lesz illetve adok x,y kordínátát a mezőknek
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {                    
                    gameBoard[i, j] = new Field(i,j);                    
                    AIBoard[i, j] = new Field(i, j, true);                    
                }

            }
            AIShipPlaced();
        }

        /// <summary>
        /// Ship is placed.
        /// </summary>
        public event EventHandler ShipPlacedEvent;

        /// <summary>
        /// Property chaned.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Shot happened.
        /// </summary>
        public event EventHandler ShotEvent;

        /// <summary>
        /// Won game.
        /// </summary>
        public event EventHandler GameEndWin;

        /// <summary>
        /// Lost game.
        /// </summary>
        public event EventHandler GameEndDefeat;

        /// <summary>
        /// Gets or sets Mouse X coordinate.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public double MouseX { get; set; }

        /// <summary>
        /// Gets or sets Mouse Y coordinate.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public double MouseY { get; set; }

        /// <summary>
        /// Gets ships.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public List<Ship> ships { get; private set; }

        /// <summary>
        /// Gets AI ships.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public List<Ship> AIShips { get; private set; }

        /// <summary>
        /// gameboard.
        /// </summary>
        public IField[,] gameBoard;

        /// <summary>
        /// Gets or sets player name.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public string PlayerName { get; set; }

        /// <summary>
        /// AI board.
        /// </summary>
        public IField[,] AIBoard;

        /// <summary>
        /// Alive ships.
        /// </summary>
        private int aliveShips;

        /// <summary>
        /// AI alive ships.
        /// </summary>
        private int AiAliveShips;

        private Direction rightDirection;
        private int[] lastHitKordinate;
        private int[] lastKordinate;
        private bool AISank;
        private bool AIS;

        /// <summary>
        /// Shot count.
        /// </summary>
        private int shotCount = 0;

        /// <summary>
        /// Logic instance.
        /// </summary>
        private readonly BattleShip.DatabaseLogic.DatabaseLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="playerName">Player name.</param>
        public GameLogic(string playerName)
        {
            logic = new DatabaseLogic.DatabaseLogic();
            App.UserName = playerName;


            var player = logic.GetUserByName(playerName);
            if (player == null)
            {
                player = new Data.Players()
                {
                    Name = playerName,
                };

                logic.Insert(player);
            }
        }

        /// <summary>
        /// Ship Rotete.
        /// </summary>
        /// <param name="ship">Get ship class and rotate clockwise.</param>
        public void Rotate(IShip ship)
        {
            switch (ship.Direction)
            {
                case Direction.up:
                    ship.Direction = Direction.right;
                    break;
                case Direction.right:
                    ship.Direction = Direction.down;
                    break;
                case Direction.down:
                    ship.Direction = Direction.left;
                    break;
                case Direction.left:
                    ship.Direction = Direction.up;
                    break;
                default:
                    break;
            }
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(gameBoard)));
        }
        
        /// <summary>
        /// Ship Place to gameboard.
        /// </summary>
        /// <param name="field">Start field where the ship placed.</param>
        /// <param name="ship">Which ship want to placed.</param>
        /// <param name="board">Gameboard where to placed the ship.</param>
        public void ShipPlaced(IField field, IShip ship, IField[,] board)
        {           
            if (enableShip(field, ship, board))
            {
                board[field.X, field.Y].ShipId = 1;                
                ship.Fields.Add(board[field.X, field.Y]);
                
                for (int i = 1; i < ship.Lenght; i++)
                {
                    int x = field.X;
                    int y = field.Y;

                    switch (ship.Direction)
                    {
                        case Direction.up:
                            board[x, y -= i].ShipId = 1;
                            ship.Fields.Add(board[x,y]);
                            break;
                        case Direction.right:
                            board[x+=i, y].ShipId = 1;
                            ship.Fields.Add(board[x, y]);
                            break;
                        case Direction.down:
                            board[x, y += i].ShipId = 1;
                            ship.Fields.Add(board[x, y]);
                            break;
                        case Direction.left:
                            board[x -= i, y].ShipId = 1;
                            ship.Fields.Add(board[x, y]);
                            break;
                        default:
                            break;
                    }                    
                }
                if (ReferenceEquals(board, gameBoard))
                {
                    ShipPlacedEvent?.Invoke(this, null);                    
                }
                
            }
           
        }

        /// <summary>
        /// Player shot method.
        /// </summary>
        /// <param name="x">Field x parameter.</param>
        /// <param name="y">Field y parameter.</param>
        /// <returns>A shot.</returns>
        public bool Shot(int x, int y)
        {
            bool pshot = false;
            

            if (AIBoard[x, y].IsShot == false)
                {
                    shotCount++;
                    pshot = true;
                    AIBoard[x, y].IsShot = true;
                    if (AIBoard[x, y].ShipId == 1)
                    {
                        foreach (var item in AIShips)
                        {
                            var hitship = item.Fields.Where(t => t == AIBoard[x, y]).FirstOrDefault();
                            if (hitship != null)
                            {
                                item.Life -= 1;
                                IsSank(item);
                            }
                        }

                    }
                }
            return pshot;
            ShotEvent?.Invoke(this, null);
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(AIBoard)));
           
                   
        }

        /// <summary>
        /// Check the ship can be put down.
        /// </summary>
        /// <param name="field">Start field where the ship placed.</param>
        /// <param name="ship">Which ship want to placed.</param>
        /// <param name="board">Gameboard where to placed the ship.</param>
        /// <returns>true or false.</returns>
        public bool enableShip(IField field, IShip ship, IField[,] board)
        {
            switch (ship.Direction)
            {
                case Direction.up:
                    if ((field.Y - (ship.Lenght - 1)) > -1)
                    {
                        for (int i = 0; i < ship.Lenght; i++)
                        {
                            if (board[field.X, field.Y - i].ShipId != 0)
                            {
                                return false;
                            }
                        }

                    }
                    else 
                    {
                        return false;
                    }
                    return true;
                case Direction.right:
                    if ((field.X + (ship.Lenght - 1)) < 10)
                    {
                        for (int i = 0; i < ship.Lenght - 1; i++)
                        {
                            if (board[field.X + i, field.Y].ShipId != 0)
                            {
                                return false;
                            }
                        }                        
                    }
                    else
                    {
                        return false;
                    }
                    return true;                  
                case Direction.down:
                    if ((field.Y + (ship.Lenght - 1)) < 10)
                    {
                        for (int i = 0; i < ship.Lenght; i++)
                        {
                            if (board[field.X, field.Y + i].ShipId != 0)
                            {
                                return false;
                            }
                        }                       
                    }
                    else 
                    {
                        return false;
                    }
                    return true;                  
                case Direction.left:
                    if ((field.X - (ship.Lenght - 1)) > -1)
                    {
                        for (int i = 0; i < ship.Lenght; i++)
                        {
                            if (board[field.X - i, field.Y].ShipId != 0)
                            {
                                return false;
                            }
                        }
                    }
                    else 
                    {
                        return false;
                    }
                    return true;
                default:
                    return false;
                   
            }                     
                                 

        }

        /// <summary>
        /// Check the ai ship is sunk.
        /// </summary>
        /// <param name="ship">IsSank.</param>
        /// <returns>If the ship sank.</returns>
        public bool IsSank(Ship ship)
        {
            if (ship.Life == 0)
            {
                foreach (var item in ship.Fields)
                {
                    item.ShipId = 3;
                }
                AiAliveShips -= 1;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(AIBoard)));
                if (AiAliveShips==0)
                {
                    GameEndWin?.Invoke(this, null);
                    return true;
                }
                return true;
            }
            else { return false; }

        }

        /// <summary>
        /// Ai placed ship to the game board.
        /// </summary>
        private void AIShipPlaced()
        {
            Random r = new Random();
            for (int i = 0; i < 5; i++)
            {
                bool enable = false;
                int x, y;
                int dir=r.Next(1, 5);
                switch (dir)
                {
                    case 1:
                        AIShips[i].Direction = Direction.up;
                        do
                        {
                            x = r.Next(0, 10);
                            y = r.Next(0, 10);
                            enable = enableShip(AIBoard[x, y], AIShips[i], AIBoard);

                        } while (enable==false);
                        ShipPlaced(AIBoard[x, y], AIShips[i], AIBoard);
                        break;
                    case 2:
                        AIShips[i].Direction = Direction.left;
                        do
                        {
                            x = r.Next(0, 10);
                            y = r.Next(0, 10);
                            enable = enableShip(AIBoard[x, y], AIShips[i], AIBoard);
                       
                        } while (enable == false);
                        ShipPlaced(AIBoard[x, y], AIShips[i], AIBoard);
                        break;
                    case 3:
                        AIShips[i].Direction = Direction.down;
                        do
                        {
                            x = r.Next(0, 10);
                            y = r.Next(0, 10);
                            enable = enableShip(AIBoard[x, y], AIShips[i], AIBoard);

                        } while (enable == false);
                        ShipPlaced(AIBoard[x, y], AIShips[i], AIBoard);
                        break;
                    case 4:
                        AIShips[i].Direction = Direction.right;
                        do
                        {
                            x = r.Next(0, 10);
                            y = r.Next(0, 10);
                            enable = enableShip(AIBoard[x, y], AIShips[i], AIBoard);

                        } while (enable == false);
                        ShipPlaced(AIBoard[x, y], AIShips[i], AIBoard);
                        break;
                    default:
                        AIShips[i].Direction = Direction.up;
                        do
                        {
                            x = r.Next(0, 10);
                            y = r.Next(0, 10);
                            enable = enableShip(AIBoard[x, y], AIShips[i], AIBoard);

                        } while (enable == false);
                        ShipPlaced(AIBoard[x, y], AIShips[i], AIBoard); 
                        break;
                }
               
            }
        }

        /// <summary>
        /// AI shot the player game board.
        /// </summary>
        public void AIShot()
        {
            
            AIS = false;
            int minAliveShipLenght = AliveShipLenght();
            if (AISank==true)
            {               
                do
                {
                    if(lastKordinate[0]>10 || lastKordinate[1]>10)
                    {
                        lastKordinate[0] = 0;
                        lastKordinate[1] = 0;
                        minAliveShipLenght = 2;
                    }

                    if ((lastKordinate[0] + lastKordinate[1]) % minAliveShipLenght == 0 && gameBoard[lastKordinate[0], lastKordinate[1]].IsShot==false)
                    {
                        AIS = true;
                        AISank = true;
                        gameBoard[lastKordinate[0], lastKordinate[1]].IsShot = true;
                        if (gameBoard[lastKordinate[0], lastKordinate[1]].ShipId == 1)
                        {
                            SubLife(lastKordinate[0], lastKordinate[1]);
                            lastHitKordinate[0] = lastKordinate[0];
                            lastHitKordinate[1] = lastKordinate[1];
                        }                      

                        ShotEvent?.Invoke(this, null);
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(gameBoard)));
                    }
                    if (lastKordinate[0] == 9 && lastKordinate[1] < 9)
                    {
                        lastKordinate[0] = -1;
                        lastKordinate[1] += 1;
                    }
                    lastKordinate[0] += 1;
                } while (AIS == false);


            }
            while (AISank == false && AIS == false)
            {
                switch (rightDirection)
                {
                    case Direction.up:
                        if(lastHitKordinate[1]>0 && gameBoard[lastHitKordinate[0], lastHitKordinate[1]-1].IsShot == false)
                        {                            
                            AIS = true;
                            gameBoard[lastHitKordinate[0], lastHitKordinate[1] - 1].IsShot = true;
                            if (gameBoard[lastHitKordinate[0], lastHitKordinate[1] - 1].ShipId == 1)
                            {
                                SubLife(lastHitKordinate[0], lastHitKordinate[1] - 1);
                                break;
                            }                         
                        }
                        rightDirection = Direction.down;
                   
                        break;
                    case Direction.down:
                        if (lastHitKordinate[1] < 9 && (gameBoard[lastHitKordinate[0], lastHitKordinate[1] + 1].IsShot == false || gameBoard[lastHitKordinate[0], lastHitKordinate[1] + 1].ShipId == 1))
                        {
                            while (gameBoard[lastHitKordinate[0], lastHitKordinate[1] + 1].IsShot == true)
                            {
                                lastHitKordinate[1] = lastHitKordinate[1] + 1;
                            }
                            AIS = true;
                            gameBoard[lastHitKordinate[0], lastHitKordinate[1] + 1].IsShot = true;
                            if (gameBoard[lastHitKordinate[0], lastHitKordinate[1] + 1].ShipId == 1)
                            {
                                SubLife(lastHitKordinate[0], lastHitKordinate[1] + 1);                               
                                break;
                            }                            
                        }
                        rightDirection = Direction.right;                     
                        break;
                    case Direction.right:
                        if (lastHitKordinate[0] < 9 && gameBoard[lastHitKordinate[0] + 1, lastHitKordinate[1]].IsShot == false) 
                        {                           
                            gameBoard[lastHitKordinate[0] + 1, lastHitKordinate[1]].IsShot = true;
                            if (gameBoard[lastHitKordinate[0]+1, lastHitKordinate[1]].ShipId == 1)
                            {
                                SubLife(lastHitKordinate[0] + 1, lastHitKordinate[1]);                                
                                AIS = true;                                
                                break;
                            }
                       
                        }
                        rightDirection = Direction.left;                        
                        break;
                    
                    case Direction.left:
                        if(lastHitKordinate[0]>0 && (gameBoard[lastHitKordinate[0] - 1, lastHitKordinate[1]].IsShot == false || gameBoard[lastHitKordinate[0]-1, lastHitKordinate[1]].ShipId == 1))
                        {
                            while (gameBoard[lastHitKordinate[0]-1, lastHitKordinate[1]].IsShot == true)
                            {
                                lastHitKordinate[0] = lastHitKordinate[0] - 1;
                            }
                            gameBoard[lastHitKordinate[0] - 1, lastHitKordinate[1]].IsShot = true;
                            if (gameBoard[lastHitKordinate[0]-1, lastHitKordinate[1]].ShipId == 1)
                            {
                                SubLife(lastHitKordinate[0] - 1, lastHitKordinate[1]);                               
                                AIS = true;                             
                                break;
                            }                           
                        }                    
                        break;
                    default:
                        AISank = true;
                        break;
                }
            }
            ShotEvent?.Invoke(this, null);
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(gameBoard)));
        }

        /// <summary>
        /// Check the ship is sunk.
        /// </summary>
        /// <param name="ship">Wich ship sunk.</param>
        /// <returns>If player's ship sank.</returns>
        public bool AiShotSank(Ship ship)
        {
            if (ship.Life == 0)
            {
                foreach (var item in ship.Fields)
                {
                    item.ShipId = 3;
                }
                rightDirection = Direction.up;
                aliveShips -= 1;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(gameBoard)));
                if (aliveShips == 0)
                {
                    GameEndDefeat?.Invoke(this, null);
                    return true;
                }
                return true;
            }
            else
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(gameBoard)));
                return false;
            }
        }

        /// <summary>
        /// Calculate the living ship maximum lenght.
        /// </summary>
        /// <returns>Int Maximum lenght ship.</returns>
        private int AliveShipLenght()
        {
            int minLenght = 10;

            foreach (var item in ships)
            {
                if(item.Life>0)
                {
                    if(minLenght>item.Lenght)
                    {
                        minLenght = item.Lenght;
                    }
                }
            }
            return minLenght;
        }

        /// <summary>
        /// Minus 1 life if the shot is found.
        /// </summary>
        /// <param name="j">Field x parameter.</param>
        /// <param name="i">Field y parameter.</param>
        private void SubLife(int j, int i)
        {
            foreach (var item in ships)
            {
                var hitship = item.Fields.Where(t => t == gameBoard[j, i]).FirstOrDefault();
                ;
                if (hitship != null)
                {
                    item.Life -= 1;
                    lastHitKordinate[0] = j;
                    lastHitKordinate[1] = i;
                    AISank = AiShotSank(item);
                }
            }
        }

        /// <summary>
        /// Calculete Score.
        /// </summary>
        /// <returns>Int Score.</returns>
        public int Score()
        {
            int score = 0;

            foreach (var item in ships)
            {
                score += item.Life * 10;
            }
            foreach (var item in AIShips)
            {
                score += (item.Lenght - item.Life) * 20;
            }

            this.logic.AddHighscore(App.UserName, score, shotCount);

            return score;
        }

        /// <summary>
        /// Gets high score by player.
        /// </summary>
        /// <returns>High score.</returns>
        public List<HighScoreByPlayer> GetHighScoreByPlayer()
        {
            return this.logic.GetHighScoresByPlayer();
           
        }
        
        /// <summary>
        /// Player name.
        /// </summary>
        /// <param name="name">Login.</param>
        public void Login(string name)
        {
            PlayerName = name;
        }

        
        /// <summary>
        /// Return coursor X koordinate.
        /// </summary>
        /// <param name="x">X.</param>
        public void GetMouseX(double x)
        {
            MouseX = x;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(gameBoard)));
        }
        
        /// <summary>
        /// Return coursor Y koordinate.
        /// </summary>
        /// <param name="y">Y.</param>
        public void GetMouseY(double y)
        {
            MouseY = y;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(gameBoard)));
        }

        private IShip GetAIShip(int x, int y)
        {
            Ship hitship = null;
            foreach (var item in AIShips)
            {
                hitship = (Ship)item.Fields.Where(t => t == gameBoard[x, y]).FirstOrDefault();
            }
            return hitship;
        }
    }
}
