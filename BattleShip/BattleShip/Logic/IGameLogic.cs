﻿using BattleShip.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip
{
    /// <summary>
    /// Interface for game logic.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Rotates ship.
        /// </summary>
        /// <param name="ship">Ship.</param>
        void Rotate(IShip ship); 

        /// <summary>
        /// Shot prop.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <returns>x, y.</returns>
        bool Shot(int x, int y);

        /// <summary>
        /// Ship placed.
        /// </summary>
        /// <param name="field">field.</param>
        /// <param name="ship">ship.</param>
        /// <param name="board">board.</param>
        void ShipPlaced(IField field, IShip ship, IField[,] board);

        /// <summary>
        /// Ai shot.
        /// </summary>
        void AIShot();
    }
}
