﻿using BattleShip.Logic;
using BattleShip.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace BattleShip
{
    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// username property.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public static string UserName = string.Empty;

        /// <summary>
        /// gets or sets Logic property.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public static GameLogic Logic { get; set; }

        /// <summary>
        ///  Gets or sets Fileds property.
        /// </summary>
        /// <value>
        /// .
        /// </value>
        public static List<Field> Fields { get; set; }

        /// <summary>
        /// Says is a ship sank.
        /// </summary>
        /// <param name="ship">Ship.</param>
        /// <returns>If a ship sank.</returns>
        public static bool IsSank(Ship ship)
        {
            if(ship.Life==0)
            {
                return true;
            }
            else { return false; }
            
        }
    }

    
}
