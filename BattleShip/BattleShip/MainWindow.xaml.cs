﻿using BattleShip.GameModels;
using BattleShip.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BattleShip
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Ship's nmber.
        /// </summary>
        public int shipNumber;

        /// <summary>
        /// Game logic instance.
        /// </summary>
        private readonly GameLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            shipNumber = 0;
            InitializeComponent();
            logic = new GameLogic();
            GM.GL = logic;           
            GM.GL.GameEndDefeat += GL_GameOverDefeat;
            GM.GL.GameEndWin += GL_GameOverVictory;
        }

        private void GL_GameOverVictory(object sender, EventArgs e)
        {
            SaveEndGame();
            this.Close();
        }

        private void GL_GameOverDefeat(object sender, EventArgs e)
        {
            SaveEndGame();
            this.Close();            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.GM.InvalidateVisual();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                var menuWindow = new MenuWindow();
                this.Close();
                menuWindow.Show();
            }

            this.GM.InvalidateVisual();
        }
       

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point po = Mouse.GetPosition(GM);                     
            int x = Convert.ToInt32(po.X) / 40;
            int y = Convert.ToInt32(po.Y) / 40;
            if (x > -1 && x < 10 && y > -1 && y < 10 && shipNumber < 5 && (GM.GL.enableShip(GM.GL.gameBoard[x, y], GM.GL.ships[shipNumber], GM.GL.gameBoard) == true))
            {
                GM.GL.ShipPlaced(GM.GL.gameBoard[x, y], GM.GL.ships[shipNumber], GM.GL.gameBoard);
                shipNumber++;
                if (shipNumber < 5)
                {
                    GM.selectedShip = GM.GL.ships[shipNumber];
                }

                if (shipNumber == 5)
                {
                    GM.selectedShip = null;
                    MessageBox.Show("Kezdődjék a háború");                    
                }
            }            
            else if (shipNumber == 5)
            {
                x = (Convert.ToInt32(po.X) - 440) / 40;
                y = Convert.ToInt32(po.Y) / 40;
                if (y > -1 && y < 10 && x > -1 && x < 10)
                {
                    if (GM.GL.Shot(x, y) == true)
                    {
                        GM.GL.AIShot();
                    }                   
                }                
            }
            else 
            {
                MessageBox.Show("Oda nem tehetsz hajót");
            }
        }

        private void Window_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {            
            if (shipNumber < 5)
            {
                GM.GL.Rotate(GM.GL.ships[shipNumber]);
                GM.selectedShip = GM.GL.ships[shipNumber];
            }           
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            Point po = Mouse.GetPosition(GM);
            int x = Convert.ToInt32(po.X);
            int y = Convert.ToInt32(po.Y);
            GM.GL.GetMouseX(x);
            GM.GL.GetMouseY(y);
        }

        /// <summary>
        /// Open the main window.
        /// </summary>
        private void SaveEndGame()
        {
            var menu = new MenuWindow();
            menu.Show();            
        }
    }
}
