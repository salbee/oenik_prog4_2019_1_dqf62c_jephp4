﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.Models
{
    /// <summary>
    /// Class of ship objects.
    /// </summary>
    public class Ship : IShip
    {
        ShipClass shipClass;
        List<IField> fields;

        /// <summary>
        /// Gets or sets field property.
        /// </summary>
        /// <value>
        /// Field property.
        /// </value>
        public List<IField> Fields {
            get { return fields; }
            set { fields = value; }
        }

        /// <summary>
        /// Gets or sets enum property.
        /// </summary>
        /// <value>
        /// Enum property.
        /// </value>
        public ShipClass ShipClass
        {
            get { return shipClass; }
            set { shipClass = value; }
        }

        int life;

        /// <summary>
        /// Gets or sets life property.
        /// </summary>
        /// <value>
        /// Life property.
        /// </value>
        public int Life
        {
            get { return life; }
            set { life = value; }
        }

        int lenght;

        /// <summary>
        /// Gets or sets lenght.
        /// </summary>
        /// <value>
        /// Length property.
        /// </value>
        public int Lenght
        {
            get { return lenght; }
            set { lenght = value; }
        }

        Direction direction;

        /// <summary>
        /// Gets or sets dirction.
        /// </summary>
        /// <value>
        /// Direction property.
        /// </value>
        public Direction Direction
        {
            get { return direction; }
            set { direction = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Ship"/> class.
        /// </summary>
        /// <param name="shipClass">Constructor.</param>
        public Ship(ShipClass shipClass)
        {
            fields = new List<IField>();
           
            this.Direction = Direction.up;
            this.ShipClass = shipClass;
            switch (shipClass)
            {
                case ShipClass.Boat:
                    this.Life = 2;
                    this.Lenght = 2;
                    break;
                case ShipClass.Submarine:
                    this.Life = 3;
                    this.Lenght = 3;
                    break;
                case ShipClass.Destroyer:
                    this.Life = 4;
                    this.Lenght = 4;
                    break;
                case ShipClass.BattleShip:
                    this.Life = 5;
                    this.Lenght = 5;
                    break;
                case ShipClass.AircraftCarrier:
                    this.Life = 6;
                    this.Lenght = 6;
                    break;
                default:
                    this.Life = 0;
                    this.Lenght = 0;
                    break;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Ship"/> class.
        /// </summary>
        public Ship()
        {
        }
    }
}
