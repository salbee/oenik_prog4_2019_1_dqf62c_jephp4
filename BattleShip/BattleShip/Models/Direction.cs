﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.Models
{
    /// <summary>
    /// Enum class for ship directions.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// Direction up,
        /// </summary>
        up,

        /// <summary>
        /// right,
        /// </summary>
        right,

        /// <summary>
        /// down,
        /// </summary>
        down,

        /// <summary>
        /// left.
        /// </summary>
        left
    }
}
