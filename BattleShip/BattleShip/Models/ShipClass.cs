﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.Models
{
    /// <summary>
    /// Ship types enum class.
    /// </summary>
    public enum ShipClass
    {
        /// <summary>
        /// Boat,
        /// </summary>
        Boat,

        /// <summary>
        /// Submarine,
        /// </summary>
        Submarine,

        /// <summary>
        /// Destroyer,
        /// </summary>
        Destroyer,

        /// <summary>
        /// BattleShip,
        /// </summary>
        BattleShip,

        /// <summary>
        /// Aircraft Carrier.
        /// </summary>
        AircraftCarrier
    }
}
