﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BattleShip.Models
{
    /// <summary>
    /// Field object's class.
    /// </summary>
    public class Field : IField
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Field"/> class.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="isAI">AI.</param>
        public Field(int x, int y, bool isAI = false)
        {
            IsShot = false;
            ShipId = 0;
            IsEnemy = false;
            X = x;
            Y = y;

            if (isAI)
            {
                FieldPiece = new Rect(440 + (X * 40), Y * 40, 40, 40);
            }
            else
            {
                FieldPiece = new Rect(X * 40, Y * 40, 40, 40);

            }
        }

        /// <summary>
        /// Gets or sets FieldPiece.
        /// </summary>
        /// <value>
        /// FieldPiece property.
        /// </value>
        public Rect FieldPiece { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a field is shot or not.
        /// </summary>
        /// <value>
        /// IsShot property.
        /// </value>
        public bool IsShot { get; set; }

        /// <summary>
        /// Gets or sets FieldPiece. If ID is 0 then the field is water otherwise ship.
        /// </summary>
        /// <value>
        /// ShipId property.
        /// </value>
        public int ShipId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a field is enemy or not.
        /// </summary>
        /// <value>
        /// IsEnemy property.
        /// </value>
        public bool IsEnemy { get; set; }

        //koordináta hogy melyik mezőről van szó (én adtam hozzá)

        /// <summary>
        /// Gets or sets X. Indicates the field's position.
        /// </summary>
        /// <value>
        /// X property.
        /// </value>
        public int X { get; set; }

        /// <summary>
        /// Gets or sets Y. Indicates the field's position.
        /// </summary>
        /// <value>
        /// Y property.
        /// </value>
        public int Y { get; set; }

    }
}
