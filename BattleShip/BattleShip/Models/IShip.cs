﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.Models
{
    /// <summary>
    /// Interface for describing ship objects.
    /// </summary>
    public interface IShip
    {
        /// <summary>
        /// Gets or sets field property.
        /// </summary>
        /// <value>
        /// Fields property.
        /// </value>
        List<IField> Fields { get; set; }

        /// <summary>
        /// Gets or sets ShipClass property.
        /// </summary>
        /// <value>
        /// ShipClass property.
        /// </value>
        ShipClass ShipClass { get; set; }

        /// <summary>
        /// Gets or sets life property.
        /// </summary>
        /// <value>
        /// Life property.
        /// </value>
        int Life { get; set; }

        /// <summary>
        /// Gets or sets lenght property.
        /// </summary>
        /// <value>
        /// Length property.
        /// </value>
        int Lenght { get; set; }

        /// <summary>
        /// Gets or sets direction property.
        /// </summary>
        /// <value>
        /// Direction property.
        /// </value>
        Direction Direction { get; set; }        
    }
}
