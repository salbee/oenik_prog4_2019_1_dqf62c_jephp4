﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BattleShip.Models
{
    /// <summary>
    /// Interface for describing field objects.
    /// </summary>
    public interface IField
    {
        /// <summary>
        /// Gets or sets a value indicating whether a ship is shot or not.
        /// </summary>
        /// <value>
        /// IsShot property.
        /// </value>
        bool IsShot { get; set; }

        /// <summary>
        /// Gets or sets FieldPiece property.
        /// </summary>
        /// <value>
        /// FieldPiece property.
        /// </value>
        Rect FieldPiece { get; set; }

        /// <summary>
        /// Gets or sets ShipID property.
        /// </summary>
        /// <value>
        /// ShiId property.
        /// </value>
        int ShipId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it's an enemy or not.
        /// </summary>
        /// <value>
        /// Fields property.
        /// </value>
        bool IsEnemy { get; set; }

        /// <summary>
        /// Gets or sets X property.
        /// </summary>
        /// <value>
        /// X property.
        /// </value>
        int X { get; set; }

        /// <summary>
        /// Gets or sets Y property.
        /// </summary>
        /// <value>
        /// Y property.
        /// </value>
        int Y { get; set; }
    }
}
