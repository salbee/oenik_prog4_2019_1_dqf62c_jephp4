﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BattleShip
{
    /// <summary>
    /// Interaction logic for MenuWindow.xaml.
    /// </summary>
    public partial class MenuWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuWindow"/> class.
        /// </summary>
        public MenuWindow()
        {
            InitializeComponent();
        }

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            this.Close();

            mainWindow.Show();
        }

        private void ScoreButton_Click(object sender, RoutedEventArgs e)
        {
            var highScoreWindow = new HighScoreWindow();
            this.Close();

            highScoreWindow.Show();
        }

        private void HelpButton_Click(object sender, RoutedEventArgs e)
        {
            var helpWindow = new HelpWindow();
            this.Close();

            helpWindow.Show();
        }
    }
}
